# Wiki Data Stream Processor

The latest version of this file can be found at the master branch of the
Arch-test repository.

## 1.0.0 (2022-08-01)

### Fixed (3 changes)

- Fixed links inside the README file
- Added Changelog
- updated basic CI/CD configuration

