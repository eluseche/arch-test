# Wiki Data Stream Processor

This project is a tiny sample live coding challenge that includes a fictional situation that sets a business use case with:

- A customer perspective of the situation
- Code to ingest data from a public stream source
- An implmentation of a Serverless HTTP API
- A Local RDBMS storage (sqlite3)
- A workflow to create an end-user report

Please read this file completely in loud voice before start doing anything.

## Running instructions :thumbsup:

- Clone this repo
- install python 3
- install pip
- install requirements.txt
- install localstack
- install serverless
- install serverless-localstack plugin
- run the local deployer
    - ./deploy.sh
- copy the **endpoint:** of the deployed API
- replace the **CLIENT_URL** in the [SSE-Client](./sse-client/wikiclient.py)
- run the sse-client
    - python sse-client/wikiclient.py

## Context 

Our company (WikiMilkersCorp) has signed a contract :smile: to analyze the bots/humans contributions to the Wiki foundation, we have to listen to every change made to any of the Wikis and make a complicated calculation if the change was made by a bot or a human, every month we have to deliver a report with the number of changes made by every group (bots/humans) to our customer who will pay for the calculations made.

The following diagram shows the product design:

```mermaid
graph LR;
    Wikimedia-->Wikiclient.py;
    Wikiclient.py<-->Calculations;
    Wikiclient.py-->Database;
    Database-->Csv;
    Csv-->Report
    Report-->Client;
```

You can see a sample report opening the [Report Template](./report-template.ods) of this project.

### Software developed

Our solution has been written twice, first version was a monolith developed in groovy, but the software developer left the company and we hired a senior developer who rewrote it completely in python, also a microservices architecture was adopted with serverless to speed up the things. The senior developr left the company after the current solution was deployed to production. Some future improvements were partially included in one of the endpoints, but, there is no documentation about it. 

The calculations are done in a HTTP API with two endpoints implemented as lambda functions [Fib](./calc/fib.py) and [Fac](./calc/fac.py) that can scale infinitely (???), the data to build the final report is stored in a local sqlite3 instance.

### C-suite concerns :worried:

- We think that we are leaving money in the table, the [Test](./sse-client/wikitester.py) shows that there is a large number of wiki modifications per minute, but we don't process as many even with the microservices approach.
- We think that the propietary calculations are unsecure
- We think that our backup strategy (copy the database to an S3 bucket) is not optimal
- We are afraid that the solution is not resilient
- We are afraid that the solution is not fault tolerant
- We are afraid that the solution is not highly available
- We don't understand how the solution is built or what changes must be made
- We think that the CI/CD capabilities are not fully used
- We are not using the Static Application Security Testing provided by our CI/CD platform

### Customer concerns :sweat:

- We think that you are not detecting correctly the number of bots/humans in the [report](./report-template.ods)?
- Are you using the [event payload](./sample-event-payload.json) sample?
- We think that you are not processing all the data that we expose
- The report looks clunky
- We don't understand how the solution is built or what changes must be made

### Operations concerns :weary:

- The workflow to create the monthly report is insanely cumbersome:
    - Run the [Data extractor](./rep-client/wikicsv.py)
    - Import the [Csv file](./output.csv) into the report template
    - Update the report template to generate the chart
    - Cleanup the data table in the database

- If we don't cleanup the database eventually the system stops working because there is too much data
- We don't understand how the solution is built or what changes must be made

## Expectations :grinning:

The main expectation that we have is that you LEAD the conversation about how to tackle this challenge.
As an architect we hope that you can address the concerns of the project's stakeholders. It means that you:

- Run the current project
- Write the project´s improvement oppportunities 
- Prioritize the improvements
- Make at least one (1) improvement
    - Create a MR, please write it well
- Create some high level technical documentation 

You decide the order of the tasks, format, standards, tools, products, team, etc. that you will use. 


## Análisis del caso de arquitectura

### Estado actual

El producto objeto de análisis cumple funcionalmente con lo que requiere el negocio. Sin embargo, el sistema responde en promedio a sólo el 1.5% de las transacciones que requieren su procesamiento. Este problema se ve reflejado en preocupaciones que ha manifestado la C-suite, dado que el ingreso más relevante de la empresa depende de este producto, directamente del número de transacciones que puede procesar por minuto. 

El cliente también han manifestado inconformidad con el número de transacciones que logra procesar la herramienta, debido a que los números reflejados en el reporte final distan de los números esperados por ellos.

Stakeholders internos y externos han manifestado otras preocupaciones en su mayoría relacionadas con desconfianza sobre la calidad del producto implementado, el cliente está dentro de los stakeholders con esta preocupación. De forma adicional se identificaron quejas relacionadas con la facilidad de operar la generación del reporte e incumplimiento de expectativas subjetivas por parte de algunos stakeholders. Las cuales se determinaron generan un menor impacto en la medición de éxito del producto y su análisis detallado será postergado para una fase posterior.

#### Componentes técnicos del producto

Luego de una reunión interna con el equipo se identificaron los siguientes componentes técnicos:
![Component diagram](WikiDataStreamProcessor.png "Component diagram - Current state")

Como se observa en el diagrama, una instancia de computo AWS EC2 se encarga de recibir los eventos del cliente. No se obtuvo información sobre su dimensionamiento actual. Esta instancia alberga un demonio escrito en python el cual siempre está a la escucha de eventos del cliente. También contiene una base de datos SQlite donde se guarda temporalmente la información resultante del procesamiento de los eventos del cliente. 

El demonio de Python que recibe la información tiene lógica de negocio que determina si una actualización enviada en un evento proviene de un ser humano o de un bot. Dependiendo de esa lógica, se llama a una u otra lambda la cual realiza un procesamiento específico para cada caso. Esas lambdas brindan una respuesta al proceso de python, el cual guarda la información en la tabla de SQLite. Se identificó que el proceso python realiza un logging activo de todos los objetos enviados por el cliente. Este logging se realiza con salida a la terminal.

Al final de cada mes, se ejecuta un proceso para exportar a csv los registros de la tabla. El archivo csv es el insumo para crear el informe requerido por el cliente y con base al cual se genera el pago a la empresa.

El equipo técnico ha manifestado que es necesario limpiar la base de datos luego de cada generación de reporte para evitar que "la base de datos se llene y afecte el funcionamiento del sistema". Se identifica que la base de datos actual cuenta con una sóla tabla, cuyos campos son de tipo numérico y cadenas de texto. 

### Hallazgos y análisis de la situación actual

El estado actual del sistema muestra una diferencia marcada, entre la capacidad que tiene de procesar eventos vs las expectativas que se tienen sobre el. Se priorizó el análisis a profundidad de este hallazgo porque afecta:
* Ingresos para la compañía
* Percepción de calidad del producto por parte del cliente

Haciendo un análisis preliminar se identifica el demonio de python como un posible cuello de botella que está afectando el número de peticiones que logra procesar el sistema. 

Esta hipótesis se basa en el hecho de tener una única máquina recibiendo los eventos y procesando la lógica de negocio puede influir en mayor medida en el número de eventos que se logran procesar por minuto. Esto se da porque se depende directamente de los recursos disponibles de la máquina para ejecutar el procesamiento de eventos. La máquina actual es de propósito general y hospeda la base de datos donde se almacenan los registros procesados, lo cual impacta en la capacidad de procesamiento de eventos total. Adicionalmente esta máquina se convierte en un punto único de fallo, afectando los niveles de tolerancia a fallos, disponibilidad y recuperación a desastres. 

Sin embargo se especificó al equipo que antes de iniciar cualquier modificación a la arquitectura actual, se deben ejecutar pruebas de rendimiento tanto en el proceso de python como en las lambdas creadas. Las pruebas se pueden realizar mediante sencillos scripts de JMeter usando mocks de data para agilizar las pruebas. La información resultante de las pruebas servirá de guía en la priorización de los proyectos de modificación que propone este ejercicio de arquitectura en la siguiente sección. Los cuales irán enfocados en aumentar la capacidad del sistema para procesar eventos por minuto.

### Proyectos de modificación en el corto plazo

Este ejercicio de arquitectura plantea varios proyectos de modificación que buscan aumentar el throughput de procesamiento de eventos del producto. La prioridad de la aplicación de estos proyectos de modificación depende de la complejidad del cambio vs el impacto esperado. Donde se busca priorizar cambios de complejidad baja que generen un alto impacto para obtener resultados lo más pronto posible, los cuales permitan apalancar proyectos de mayor complejidad. 

#### Refactorizar wikiclient.py

El protocolo SSE requiere que el cliente esté activamente escuchando los eventos. En el corto plazo se mantendrá la máquina actual EC2 con el demonio wikiclient.py, sin embargo sólo será responsable de escuchar los eventos. La función "process" se migrará a una lambda en python que insertará la información en AWS DynamoDB. La lógica de negocio y llamados a las otras lambdas se mantendrán igual por ahora mientras se obtiene mayor entendimiento de su funcionamiento. El llamado de wikiclient.py a la nueva lambda será asíncrono sin esperar respuesta de parte de ella.
La exportación de datos al CSV se puede hacer directamente desde AWS DynamoDB evitando tener que migrar el proceso de exportación a csv. 
Estos cambios deberán quedar documentados en los repositorios de la empresa, incluyendo diagrama de componentes y de secuencia. 
- Complejidad del cambio: Media.
- Impacto esperado: Muy alto.
- Resultados esperados: Procesamiento del 100% de los eventos enviados por el cliente y reflejados en el reporte final. Aumento en los niveles de disponibilidad del servicio. Mejora en los procesos operativos de creación del reporte.
Nota: Si se determina que el nivel de indisponibilidad del servicio supera un umbral soportable se deberá migrar a una instancia en AWS ECS para reducir los tiempos de indisponibilidad del servicio. 

#### Refactorizar lambdas fib y fac.

En el ejercicio de levantamiento de información el equipo manifestó que las lambdas fib y fac realizaban procesamientos complejos que parecían tomar mucho tiempo. No se cuenta con información que lo corrobore, sin embargo se propone ejecutar pruebas de rendimiento para validar el impacto de estas funciones en el rendimiengo global del sistema. Dado que de comprobarse que el impacto es alto o muy alto se podría obtener una ganacia rápida, dado que la complejidad estimada de un refactor a nivel de código fuente tiende a ser baja.
- Complejidad del cambio: Baja.
- Impacto esperado: Por definir.
- Resultados esperados: Aumento en el número de eventos procesados por minuto. 

#### Mejorar manejo de logging

Se recomienda tan pronto como sea posible hacer logging únicamente de lo estrictamente necesario para identificar si los eventos están ocurriendo correctamente. Evitando incluir información no necesaria del objeto recibido. Adicionalmente se propone dirigir la salida del registro a disco duro configurando la rotación de archivo en un día, verificando si el tamaño del registro se mantiene en números razonables.
- Complejidad del cambio: Muy baja.
- Impacto esperado: Medio.
- Resultados esperados: Reduce el tiempo de escritura de logs, disminuyendo el riesgo de caida del servicio por llenado de disco duro.

### Siguientes pasos

Luego de identificar con datos el origen u orígenes de la diferencia entre eventos recibidos vs eventos procesados, se determinará la prioridad en la cual se deben abordar los proyectos propuestos en el corto plazo. 

Mientras estos proyectos inician su implementación, se debe verificar si la solución propuesta que busca lograr el 100% de procesamiento de los eventos logra el resultado esperado o si es necesario pivotar a una solución alternativa.

Adicionalmente es necesario indagar con mayor profundidad las demás preocupaciones de los stakeholders, definiendo los requerimientos no funcionales relacionados con dichas preocupaciones e identificando cuales de esos requerimientos serán los drivers de la arquitectura final. En este ejercicio de arquitectura se identificaron los siguientes requerimientos no funcionales deseados por parte de los stakeholders:
- Rendimiento
- Mantenibilidad
- Alta disponibilidad
- Tolerancia a fallos
- Resiliencia del sistema
- Seguridad
- Confidencialidad
- Confiabilidad

En paralelo el equipo podrá investigar acerca de las mejores prácticas para construir clientes usando el protocolo SSE. Dado que en el momento en que se realizó este ejercicio de arquitectura nadie del equipo contaba con experiencia previa en implementaciones usando dicho protocolo.


## Architecture case study analysis

### Current state

The product object of analysis complies with what the business requires on the functional requirements. However, the system responds to only 1.5% of the transactions that require processing on average. This problem is reflected in the concerns from the C-suite, given that the most relevant income of the company depends on this product, specifically on the number of transactions that it can process per minute.

The client has also expressed disagreement with the number of transactions the product is able to process, due the numbers reflected in the final report are far from the numbers expected by them.

Internal and external stakeholders have expressed other concerns, mostly related to mistrust about the quality of the implemented product, the client is among the stakeholders with this concern. Additionally, complaints were identified related to the ease of operating the generation of the report and non-compliance with subjective expectations by some stakeholders. Which were determined generate less impact on the measurement of success of the product and its detailed analysis will be postponed for a later phase.

#### Technical components of the product

After an internal meeting with the team, the following technical components were identified:
![Component diagram](WikiDataStreamProcessor.png "Component diagram - Current state")

As can be seen in the diagram, an AWS EC2 compute instance is responsible for receiving client events. No information was obtained on its current sizing. This instance hosts a daemon written in python which is always listening for client events. It also contains a SQLite database where the information resulting from the processing of client events is temporarily stored.

The Python daemon that receives the information has business logic to determine whether an update sent in an event is from a human or a bot. Depending on that logic, one or another lambda is called which performs a specific processing for each case. Those lambdas return a response to the python process, which saves the information to the SQLite table. It has been identified that the python process actively logs all objects sent by the client. This logging is done sending the output to the terminal.

At the end of each month, a process is run to export the records in the table to csv. The csv file is the input to create the report required by the client and based on which the payment to the company is generated.

The technical team has stated that it is necessary to clean the database after each report generation to prevent "the database from filling up and affecting the functioning of the system." It is identified that the current database has a single table, whose fields are of type numeric and strings.

### Findings and analysis of the current situation

The current state of the system shows a clear difference between its ability to process events vs. the expectations that are placed on it. An in-depth analysis of this finding was prioritized because it affects:
* Income for the company
* Perception of product quality by the client

Doing a preliminary analysis, the python daemon is identified as a possible bottleneck that is affecting the number of requests that the system is able to process.

This hypothesis is based on the fact that having a single machine receiving the events and processing the business logic can have a greater influence on the number of events that can be processed per minute. This is because it depends directly on the available resources of the machine to execute the event processing. The current machine is general purpose and hosts the database where the processed records are stored, which impacts the total event processing capacity. Additionally, this machine becomes a single point of failure, affecting the levels of fault tolerance, availability and disaster recovery.

However, the team was specified that before starting any modifications to the current architecture, performance tests should be run on both the python process and the lambdas created. Testing can be done through simple JMeter scripts using data mocks to speed up testing. The information resulting from the tests will serve as a guide in prioritizing the modification projects proposed by this architecture exercise in the next section. Which will be focused on increasing the system's capacity to process events per minute.

### Short-term modification projects

This architecture exercise proposes several modification projects that seek to increase the event processing throughput of the product. The priority of the application of these modification projects depends on the complexity of the change vs. the expected impact. Where it is sought to prioritize changes of low complexity that generate a high impact to obtain results as soon as possible, which allow leveraging projects of greater complexity.

#### Refactor wikiclient.py

The SSE protocol requires that the client has been actively listening for events. In the short term the current EC2 machine with the wikiclient.py daemon can be kept, however it will only be responsible for listening to events. The "process" function will be migrated to a python lambda that will push the data into AWS DynamoDB. The business logic and calls to the other lambdas will stay the same for now while the team gain a better understanding of how it works. The call from wikiclient.py to the new lambda will be asynchronous without waiting for a response from it.
Data export to CSV can be done directly from AWS DynamoDB avoiding migration of the exporting process to csv.
These changes must be documented in the company's repositories, including a component and sequence diagram.
- Change complexity: Medium.
- Expected impact: Very high.
- Expected results: Processing of 100% of the events sent by the client and reflected in the final report. Increase in service availability levels. Improvement in the operational processes of creating the report.
Note: If it is determined that the level of unavailability of the service exceeds a bearable threshold, the EC2 instance should to be migrated to an instance in AWS ECS to reduce unavailability of the service.

#### Refactor fib and fac lambdas.

In the information-collecting exercise, the team stated that the fib and fac lambdas performed complex processing that seemed to take a long time. There is no information to corroborate it, however it is proposed to run performance tests to validate the impact of these functions on the overall performance of the system. If the impact is found to be high or very high, a quick win could be obtained, since the estimated complexity of a refactor at the source code level tends to be low.
- Change complexity: Low.
- Expected impact: To be defined.
- Expected results: Increase in the number of events processed per minute.

#### Improve log handling

It is recommended as soon as possible to log only what is strictly necessary to identify if the events are occurring correctly. Avoiding including unnecessary information of the received object. Additionally, it is proposed to send the log output to hard disk, setting the file rotation in one day, verifying if the size of the log is kept in reasonable numbers.
- Change complexity: Very low.
- Expected impact: Medium.
- Expected results: Reduces log writing time, reducing the risk of service failure due to hard disk filling.

### Next steps

After identifying with data the origin or origins of the difference between received events vs. processed events, the priority in which the proposed projects should be addressed in the short term will be determined.

While these projects begin their implementation, it must be verified if the proposed solution that seeks to achieve 100% event processing achieves the expected result or if it is necessary to pivot to an alternative solution.

Additionally, it is necessary to investigate carefully the other concerns from the stakeholders, defining non-functional requirements related to these concerns and identifying which of these requirements will be the drivers of the final architecture. In this architecture analysis, the following non-functional requirements wanted by the stakeholders were identified:
- Performance
- Maintainability
- High availability
- Fault tolerance
- System resilience
- Security
- Confidentiality
- Reliability

In parallel, the team will be able to investigate best practices for building clients using SSE protocol. Since at the time this architecture exercise was carried out, no one on the team had previous experience in implementations using said protocol.