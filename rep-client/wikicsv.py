import csv
import json
import sqlite3
import logging

DB = "wikimoney.db"

def generate_csv():
    try:
        con = sqlite3.connect(DB)
        cursorObj = con.cursor()
        cursorObj.execute("""
            --How to know if the user was a bot or not?
            select user, COUNT(*)
            from wiki_updates wu
            group by 1
            order by 2 desc
            ;
        """)
        with open('output.csv','w') as out_csv_file:
            csv_out = csv.writer(out_csv_file)
            # write header                        
            csv_out.writerow([d[0] for d in cursorObj.description])
            # write data                          
            for result in cursorObj:
                csv_out.writerow(result)
    except Exception as e:
        print(e) 

generate_csv()