import json
import sqlite3
import logging
import asyncio
import aiohttp
import requests
from datetime import datetime
from aiosseclient import aiosseclient

async def log(event):
    print("Event received from user: %s" % json.loads(event.data)['user'])

async def main():
    async for event in aiosseclient('https://stream.wikimedia.org/v2/stream/recentchange'):
        await log(event)

loop = asyncio.get_event_loop()
loop.run_until_complete(main())